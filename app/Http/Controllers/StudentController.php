<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Student as Student;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\File;

class StudentController extends Controller {

	public function create() {
		return view('create');
	}

	public function store(Request $request) {

		if ($request->hasfile('image')) {
			$file = $request->file('image');
			$name = time() . $file->getClientOriginalName();
			$file->move(public_path() . '/images/', $name);
		}

		$validator = Validator::make($request->all(), [
				'first_name' => 'required',
				'last_name' => 'required',
				'email' => 'required',
				'number' => 'max:255',
				'address' => 'required',
				'gender' => 'required',
		]);

		if ($validator->fails()) {
			return redirect()->back()
					->withErrors($validator)
					->withInput();
		}
		$student = new Student($request->all());
		$student->image = isset($name) ? $name : 'no image';
		$student->status = $request->get('status') ? $request->get('status') : 0;

		$student->save();
		return redirect('students')->with('success', 'Student added');
	}

	public function index() {
		$q = Input::get('q');
		$students = Student::where('first_name', 'LIKE', '%' . $q . '%')->orWhere('email', 'LIKE', '%' . $q . '%')->paginate(10);

		if (count($students) > 0) {
			return view('index', compact('students'))->withDetails($students)->withQuery($q);
		} else {
			return view('index', compact('students'))->withMessage('No Details found. Try to search again !');
		}
	}

	public function edit($id) {
		$student = student::find($id);
		return view('edit', compact('student', 'id'));
	}

	public function update(Request $request, $id) {

		$student = student::find($id);
		if (Input::hasFile('image')) {
			$usersImage = public_path("images/{$student->image}");
			if (File::exists($usersImage)) {
				unlink($usersImage);
			}
			$file = Input::file('image');
			$name = time() . '-' . $file->getClientOriginalName();
			$file->move(public_path() . '/images/', $name);
		}

		$student->first_name = $request->get('first_name');
		$student->last_name = $request->get('last_name');
		$student->email = $request->get('email');
		$student->number = $request->get('number');
		$student->address = $request->get('address');
		Input::hasFile('image') ? $student->image = $name : $student->image;
		$student->status = $request->get('status') ? $request->get('status') : 0;
		$student->save();
		return redirect('students')->with('success', 'Student  updated');
		
	}

	public function destroy($id) {
		$student = student::find($id);
		$student->delete();
		return redirect('students')->with('success', 'Student  deleted');
	}

	public function show($id) {
		$student = Student::find($id);
		return view('show', compact('student', 'id'));
	}

}
