<head>
    <meta charset="utf-8">
    <title>Student Details</title>
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
  </head>   
  <div class="center">
<div class="row">
        <div class="load">
            <div class="pull-left">
                <h2> Student Detail</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('students.index') }}"> Back</a>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="load">
            <div class="form-group">
                <strong>First Name:</strong>
                {{ $student->first_name}}
            </div>
        </div>
		<div class="load">
            <div class="form-group">
                <strong>Last Name:</strong>
                {{ $student->last_name}}
            </div>
        </div>
        <div class="load">
            <div class="form-group">
                <strong>Email:</strong>
                {{ $student->email}}
            </div>
        </div>
		<div class="load">
            <div class="form-group">
                <strong>Number:</strong>
                {{ $student->number}}
            </div>
        </div>
		<div class="load">
            <div class="form-group">
                <strong>Address:</strong>
                {{ $student->address}}
            </div>
        </div>
		<div class="load">
            <div class="form-group">
                <strong>Gender:</strong>
                {{ $student->gender}}
            </div>
        </div>
		<div class="load">
            <div class="form-group">
                <strong>Student Picture:</strong>
		<img src="{{ asset('images/'.$student->image) }}" />
            </div>
        </div>
    </div>
</div>