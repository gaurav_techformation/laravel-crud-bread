<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Update Student</title>
		<link rel="stylesheet" href="{{asset('css/app.css')}}">
	</head>
	<body>
		<div class="container">
			<h2>Update Student Details</h2><br  />
			<div class="pull-right">
                <a class="btn btn-primary" href="{{ route('students.index') }}"> Back</a>
            </div>
			<form method="post" action="{{action('StudentController@update', $id)}}" enctype="multipart/form-data">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">      
				<input name="_method" type="hidden" value="PATCH">
				<div class="row">
					<div class="col-md-4"></div>
					<div class="form-group col-md-4">
						<label for="First Name">First Name:</label>
						<input type="text" class="form-control" name="first_name" value="{{$student->first_name}}" required>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4"></div>
					<div class="form-group col-md-4">
						<label for="Last Name">Last Name</label>
						<input type="text" class="form-control" name="last_name" value="{{$student->last_name}}" required>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4"></div>
					<div class="form-group col-md-4">
						<label for="Email">Email:</label>
						<input type="email" class="form-control" name="email" value="{{$student->email}}" required>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4"></div>
					<div class="form-group col-md-4">
						<label for="Numebr">Number:</label>
						<input type="text" class="form-control" name="number" value="{{$student->number}}" required>
					</div>
				</div>

				<div class="row">
					<div class="col-md-4"></div>
					<div class="form-group col-md-4">
						<label for="Address">Address:</label>
						<input type="text" class="form-control" name="address" value="{{$student->address}}" required>
					</div>
				</div>

				<div class="row">
					<div class="col-md-4"></div>
					<div class="form-group col-md-4">
						<lable>Status:</lable>
						<?php
						if ($student->status == 1) {
							$checked = "checked";
						} else {
							$checked = '';
						}
						?>
						<input type="checkbox"  name="status" id="status" value="{{$student->status}}"  <?= $checked ?>>Active
					</div>
				</div>

				<div class="row">
					<div class="col-md-4"></div>
					<div class="form-group col-md-4">
						<img src="{{ asset('images/'.$student->image) }}" />
						<input type="file" name="image" > Upload New Picture   
					</div>
				</div>
				<div class="row">


					<div class="col-md-4"></div>
					<div class="form-group col-md-4" style="margin-top:60px">
						<button type="submit" class="btn btn-success" style="margin-left:38px">Update</button>
					</div>
				</div>
			</form>
		</div>
	</body>
</html>
