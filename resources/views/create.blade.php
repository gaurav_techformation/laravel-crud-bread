<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Create Student</title>
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">  
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.css" rel="stylesheet">  
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>  
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>  
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.js"></script>  
  </head>
  <body>
    <div class="container">
      <h2>Create Student</h2><br/>
	  <a href="{{action('StudentController@index')}}" class="btn btn-primary">Home</a>
      <form method="post" action="{{url('students')}}" enctype="multipart/form-data">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">      
		<div class="row">
          <div class="col-md-4"></div>
          <div class="form-group col-md-4">
            <label for="First Name">First Name:</label>
            <input type="text" class="form-control" name="first_name" required>			
          </div>
        </div>
        <div class="row">
          <div class="col-md-4"></div>
            <div class="form-group col-md-4">
              <label for="Last Name">Last Name:</label>
              <input type="text" class="form-control" name="last_name" required>
            </div>
          </div>
        <div class="row">
          <div class="col-md-4"></div>
            <div class="form-group col-md-4">
              <label for="Email">Email:</label>
              <input type="email" class="form-control" name="email" required>
            </div>
          </div>

 <div class="row">
          <div class="col-md-4"></div>
            <div class="form-group col-md-4">
              <label for="Number">Number:</label>
              <input type="text" class="form-control" name="number" required>
            </div>
          </div>
		   <div class="row">
          <div class="col-md-4"></div>
            <div class="form-group col-md-4">
              <label for="Address">Address:</label>
              <input type="text" class="form-control" name="address" required>
            </div>
          </div>
		<div class="row">
			  <div class="col-md-4"></div>
			  <div class="form-group col-md-4">
				<input type="file" name="image">    
			 </div>
			</div>
      <div class="row">
          <div class="col-md-4"></div>
            <div class="form-group col-md-4">
                <lable>Gender</lable>
				<input type="radio" id="sex" name="gender" value="female" />Female
				<input type="radio" id="sex" name="gender" value="male" />Male
            </div>
        </div>
	<div class="row">
          <div class="col-md-4"></div>
            <div class="form-group col-md-4">
                <lable>Status:</lable>
		 <input type="checkbox" name="status" id="status" value="1">Active
            </div>
        </div>
        </div>
        <div class="row">
          <div class="col-md-4"></div>
          <div class="form-group col-md-4" style="margin-top:60px">
            <button type="submit" class="btn btn-success">Submit</button>
          </div>
        </div>
      </form>
    </div>
  </body>
</html>
