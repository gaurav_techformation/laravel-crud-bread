	
<html>
  <head>
	   <script src="jquery-3.3.1.min.js"></script>
    <meta charset="utf-8">
    <title>Home</title>
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
  </head>
  <body>
	  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
<script src="js/scripts.js"></script>
	 <form action="" method="get" role ="search" >
<input type="hidden" name="_token" value="{{ csrf_token() }}">      
    <div class="input-group">
        <input type="text" class="form-control" name="q" placeholder="Search students" value="{{(isset($_GET['q']) ? ($_GET['q']) : '')}}"> 
		<span class="input-group-btn">
            <button type="submit" class="btn btn-default">
                <span class="glyphicon glyphicon-search"></span>
            </button>
        </span>
    </div>
</form>
    <div class="container">
		 <br />
    @if (\Session::has('success'))
      <div class="alert alert-success">
        <p>{{ \Session::get('success') }}</p>
      </div><br />
     @endif
		 @if(isset($details))
    <br />
    <table class="table table-striped">
	<a href="{{action('StudentController@create')}}" class="btn btn-primary">Create Student</a>

    <thead>
      <tr>
        <th>ID</th>
        <th>First Name</th>
        <th>Last name</th>
        <th>Email</th>
        <th>Number</th>
        <th>Address</th>
        <th colspan="2">Action</th>
      </tr>
    </thead>
    <tbody>
      
      @foreach($details as $student)
      <tr>
        <td>{{$student['id']}}</td>
        <td>{{$student['first_name']}}</td>
        <td>{{$student['last_name']}}</td>
        <td>{{$student['email']}}</td>
        <td>{{$student['number']}}</td>
        <td>{{$student['address']}}</td>

        
		 <td><a href="{{action('StudentController@show', $student['id'])}}" class="btn btn-warning">View</a></td>
        <td><a href="{{action('StudentController@edit', $student['id'])}}" class="btn btn-warning">Edit</a></td>
        <td>
          <form class="delete"  action="{{action('StudentController@destroy', $student['id'])}}" method="post">
<input type="hidden" name="_token" value="{{ csrf_token() }}">      
            <input name="_method" type="hidden" value="DELETE">
            <button class="btn btn-danger" type="submit">Delete</button>
          </form>
        </td>
      </tr>
      @endforeach
	  <script>
    $(".delete").on("submit", function(){
        return confirm("Do you want to delete this item?");
    });
</script>
    </tbody>
  </table>
	     {{ $details->links() }}	
		 @endif
  </div>
  </body>
</html>